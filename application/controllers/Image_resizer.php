<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Image_resizer extends CI_Controller {
	public $unsecure_methods = [], 
    $loggedin_user, 
    $header_token_name,
    $access_log_insert_id;


    public function __construct() {
        parent::__construct();
        $this->header_token_name = 'device-token';
        
        $this->load->model('api_model');

        // list all methods which not required device-token
        $this->unsecure_methods = [
            'login',
        ];

        //log api access
        $this->access_log_insert_id = $this->api_model->log_api_access();

        // Check login
        $this->loggedin_user = $this->api_model->check_api_auth();
        if($this->loggedin_user === false) {
            $this->send_response([
                'status'=>FALSE,
                'message'=>'You do not have sufficient permission to access this resources.'
            ]);
        }
    }

    public function send_response($data) {
        
        // log response data
        $this->db->where('id', $this->access_log_insert_id);
        $this->db->update('api_access',['response_send'=>json_encode($data)]);

        header('Content-Type: application/json');
        echo json_encode($data);die;
    }


    //send otp
    public function login() {
        //validation
        $this->load->library('form_validation');
        $this->form_validation->set_rules('login_username','Username','required');
        $this->form_validation->set_rules('password','Password','required');

        if($this->form_validation->run() == FALSE) {
            $this->send_response([
                'status' => FALSE,
                'message' => strip_tags(validation_errors())
            ]);
        }
       
        $login_username=$this->input->post('login_username');
        $password=$this->input->post('password');
        $post_array=$this->input->post();

        $login_user = $this->api_model->check_login($login_username, $password);

        if($login_user === false) {
			$this->send_response([
                'status' => FALSE,
                'message' => "Username or password is wrong."
            ]);
        }

        // create new device token
        $device_data=array(
            'user_id'=>$login_user['user_id'],
            'device_token'=>$this->api_model->generate_api_token(),
            'login_at'=>date('Y-m-d H:i:s'),
            'is_active'=>'1',
        );
        $this->db->insert('app_user_login', $device_data);

        // do not send password in response
        unset($login_user['password']);

        $this->send_response([
            'status'=>true,
            'message'=>'Loggedin successfully',
            'data'=>[
            	$this->header_token_name=>$device_data['device_token'],
            	'userdata'=>$login_user
            ],
        ]);
    }

    public function resize_image() {
    	

    	$this->load->library('form_validation');
        $this->form_validation->set_rules('image_url','Image Url','required');
        $this->form_validation->set_rules('height','Height','required|integer');
        $this->form_validation->set_rules('width','Width','required|integer');

        if($this->form_validation->run() == FALSE) {
            $this->send_response([
                'status' => FALSE,
                'message' => strip_tags(validation_errors())
            ]);
        }
        $image_url = $this->input->post('image_url');
        $width = $this->input->post('image_url');
        $height = $this->input->post('height');

    	// image_url should be valid url
    	if(!filter_var($image_url, FILTER_VALIDATE_URL)) {
    		$this->send_response([
	            'status'=>false,
	            'message'=>'Provided image_url is not valid URL.',
	        ]);
    	}

    	// validate if image or not
    	if(!$image_info = is_valid_image($image_url)) {
    		$this->send_response([
	            'status'=>false,
	            'message'=>'Provided image_url is not contain valid image.',
	        ]);
    	}

    	$file_extension = explode('/', $image_info['mime']);
    	$put_file = $this->api_model->get_unique_upload_filename(IMG_DOWNLOAD_PATH,$file_extension[1]);

    	// fail to put content
    	if(file_put_contents($put_file, fopen($image_url, 'r')) === false) {
    		$this->send_response([
	            'status'=>false,
	            'message'=>'Can not read data from URL.',
	        ]);
    	}

    	// resize image
    	$resize_image_path = $this->api_model->resize_image($put_file, $width, $height);

    	// put it on aws
    	$this->load->model('aws_storage');
    	$this->aws_storage->aws_filesystem->put('image_resizer/'.basename($resize_image_path),file_get_contents($resize_image_path));

    	// delete both the images
    	unlink($resize_image_path);
    	unlink($put_file);
    	
    	$this->send_response([
            'status'=>false,
            'message'=>'Image resized and uploaded on S3',
        ]);
    }

}