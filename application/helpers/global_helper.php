<?php

function get_header_param($key) {
	$header_param = getallheaders();
	$header_param = array_change_key_case($header_param);
	if(isset($header_param[$key])) {
		return $header_param[$key];
	}
	return null;
}

// get random
function get_random_string($length=8) {
	$md5=md5(uniqid(rand(), true));
	return substr($md5,2,$length);
}

function download_file_from_url($url) {
	
}


// return false if not image else return getimagesize data
function is_valid_image($url) {
	$size = getimagesize($url);
    if(!$size) {
        return 0;
    }

    $valid_types = array(IMAGETYPE_JPEG, IMAGETYPE_PNG);

    if(!in_array($size[2],  $valid_types)) {
        return false;
    }
    return $size;
}
