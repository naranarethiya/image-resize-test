<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

use Aws\S3\S3Client;
use League\Flysystem\AwsS3v3\AwsS3Adapter;
use League\Flysystem\Filesystem;

class Aws_storage extends CI_Model
{
    public $aws_config,
    $aws_bucket,
    $aws_client,
    $aws_adapter,
        $aws_filesystem;

    public function __construct()
    {
        $this->aws_config = [
            'credentials' => [
                'key' => '',
                'secret' => '',
            ],
            'region' => '',
            'version' => 'latest',
        ];
        $this->aws_bucket = '';
        
        $this->aws_client = new S3Client($this->aws_config);
        $this->aws_adapter = new AwsS3Adapter($this->aws_client, $this->aws_bucket);
        $this->aws_filesystem = new Filesystem($this->aws_adapter);

        // Available methods
        //$data = $this->aws_filesystem->has('composer/composer.json');
        //$data = $this->aws_filesystem->put('composer/composer.json', file_get_contents(__DIR__.'/composer.json'));
        //$data = $this->aws_filesystem->read('composer/composer.json');
        //$data = $this->aws_filesystem->rename('composer/composer.json','composer/composer.json');
        //$data = $this->aws_filesystem->copy('composer/composer.json','composer.json');
        //$data = $this->aws_filesystem->delete('composer/composer.json');
        //$data = $this->aws_filesystem->listContents();
    }

    // Generate temp url
    // @key - File path
    // @time - Timestamp till the URL will be valid
    public function temporaryUrl($key, $time = false)
    {
        $key = str_replace('//', '/', $key);
        $command = $this->aws_client->getCommand('GetObject', array_merge([
            'Bucket' => $this->aws_bucket,
            'Key' => $key,
        ]));

        if ($time == false) {
            $time = strtotime('+5 minutes');
        }
        return (string) $this->aws_client->createPresignedRequest($command, '+20 minutes')->getUri();
    }

    public function objectExist($key)
    {
        $key = str_replace('//', '/', $key);
        $command = $this->aws_client->getCommand('GetObject', array_merge([
            'Bucket' => $this->aws_bucket,
            'Key' => $key,
        ]));

        if ($time == false) {
            $time = strtotime('+5 minutes');
        }
        return (string) $this->aws_client->createPresignedRequest($command, '+20 minutes')->getUri();
    }

}
