<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
  
class Api_model extends CI_Model {
	public function __construct() {
		parent::__construct();
	}


	// Check and return user row if logged in
	public function check_api_auth($method = false) {

		if(!$method) {
			$method = $this->uri->segment(2);
		}
		
		// if non secure method than return true
		if(in_array($method, $this->unsecure_methods)) {
			return true;
		}

		// check validity of token
		$device_token = get_header_param('device-token');
        

        // get active token
        $this->db->where('device_token',$device_token);
        $this->db->where('is_active','1');
        $user_data=$this->get_user_with_login();
        

        if(empty($user_data)) {
            return false;
        }

        return $user_data[0];
	}

	public function log_api_access() {

		$loggedin_user = $this->check_api_auth();
		$device_id = 0;
		if(!empty($loggedin_user)) {
			$device_id = $loggedin_user['device_id'];
		}


		$insert_array = [
			'device_id'=>$device_id,
			'api_url'=>uri_string(),
			'request_header'=>json_encode(getallheaders()),
			'request_data'=>json_encode($_POST),
			'response_send'=>'',
			'created_at'=>date('Y-m-d H:i:s'),
		];

		$this->db->insert('api_access', $insert_array);
		return $this->db->insert_id();
	}

	public function check_login($login_username, $password) {
		$this->db->where('login_username', $login_username);
		$this->db->where('password', md5($password));
		$rs = $this->db->get('user');
		$result_array = $rs->result_array();

		if(empty($result_array)) {
			return false;
		}
		return $result_array[0];
	}

	public function generate_api_token() {
		$token=get_random_string(20);
		$device_data=$this->get_api_token_data($token);
		if(empty($device_data)) {
			return $token;
		}

		return generate_token();
	}

	public function get_api_token_data($token){
		$this->db->where('device_token', $token);
		$rs = $this->db->get('app_user_login');
		return $rs->result_array();
	}

	public function get_user_with_login() {
		$this->db->select('app_user_login.*,user.*');
		$this->db->join('app_user_login','app_user_login.user_id=user.user_id');
		$rs=$this->db->get('user');
		return $rs->result_array();
	}

	public function resize_image($file_path, $width, $height) {
		
		$config['image_library'] = 'gd2';
		$config['source_image'] = $file_path;
		$config['maintain_ratio'] = TRUE;
		//$config['create_thumb'] = TRUE;
		$config['width'] = $width;
		$config['height'] = $height;
		$config['new_image'] = IMG_RESIZED_PATH.DIRECTORY_SEPARATOR.basename($file_path);
		$this->load->library('image_lib', $config);

		if(!$this->image_lib->resize()) {
			return false;
		}

		return $config['new_image'];
	}

	public function get_unique_upload_filename($dir,$file_extension) {
		$unique_flag = false;
		$dir = rtrim($dir,DIRECTORY_SEPARATOR);

		while ($unique_flag == false) {
			$file_name = get_random_string(10).'.'.$file_extension;
			$full_path = $dir.DIRECTORY_SEPARATOR.$file_name;
			if(!file_exists($full_path)) {
				$unique_flag = true;
			}
		}

		return $full_path;
	}

}