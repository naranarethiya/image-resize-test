##############
Configuration
##############

 1. **IMG_DOWNLOAD_PATH** -  in *application/constants.php* Set Proper folder path where images suppose to be store, do create folder and give proper permissions
 2. **IMG_RESIZED_PATH** -  in *application/constants.php* Set Proper folder path where resized images suppose to be store, do create folder and give proper permissions
 3. **AWS configuratin** - in *application/models/Aws_storage.php* set your AWS configuration(key, secret and bucket).
 4. **Database Config** - Import *other/image-resize-test.sql* file and change you database settings into *application/config/database.php*
 5. **composer** - fire command composer install and composer dump-autoload to download AWS client lib.


##############
API Method
##############
For testing application use following details, else create your own with phpmyadmin. Make sure password is **encrypted with MD5**

 - Username : naranarethiya
 - Password : 1234567

## 1) image_resizer/login
Login method to get api token
 - Parameter : login_username, password 
 - Response : device-token, userdata

## 2) image_resizer/resize_image
Resize image and upload it not AWS. 

**device-token** header is required with value return by **image_resizer/login**
 - Parameter : image_url, height, width
 - Response : nothing


